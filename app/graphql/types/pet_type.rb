module Types
  class PetType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :description, String, null: false
    field :stock, Integer, null: false
    field :price, Float, null: false
  end
end
