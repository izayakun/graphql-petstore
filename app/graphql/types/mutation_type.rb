module Types
  class MutationType < BaseObject
    field :create_order, mutation: Mutations::CreateOrder
  end
end
