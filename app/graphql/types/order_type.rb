module Types
  class OrderType < Types::BaseObject
    field :id, ID, null: false
    field :shipping_address, String, null: false
    field :quantity, Integer, null: false
    field :amount, Float, null: false
    field :status, String, null: false
  end
end
