module Mutations
  class CreateOrder < BaseMutation
    # arguments passed to the `resolve` method
    argument :shipping_address, String, required: true
    argument :quantity, String, required: true
    argument :amount, String, required: true

    # return type from the mutation
    type Types::OrderType

    def resolve(shipping_address: nil, quantity: nil, amount: nil)
      Order.create!(
        shipping_address: shipping_address,
        quantity: quantity,
        amount: amount
      )
    end
  end
end
