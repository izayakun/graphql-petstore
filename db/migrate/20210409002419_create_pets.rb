class CreatePets < ActiveRecord::Migration[6.1]
  def change
    create_table :pets do |t|
      t.string :name
      t.string :desription
      t.integer :stock
      t.float :price

      t.timestamps
    end
  end
end
