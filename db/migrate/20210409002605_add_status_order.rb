class AddStatusOrder < ActiveRecord::Migration[6.1]
  def change
    change_table :orders do |t|
      t.string :status, default: 'Pending'
    end
  end
end
