class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.string :shipping_address
      t.integer :quantity
      t.float :amount

      t.timestamps
    end
  end
end
